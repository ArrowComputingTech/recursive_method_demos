#!/home/hz/.rbenv/shims/ruby

def fibs(n)
  return 0 if n == 0
  n == 1 ? 1 : fibs(n - 1) + fibs(n - 2)
end

puts fibs(5)
puts fibs(6)
