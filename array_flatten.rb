#!/home/hz/.rbenv/shims/ruby

def squash(arr, result = [])
  arr.each do |value|
    value.kind_of?(Array) ? squash(value, result) : result << value
  end
  result
end

arr = [[1,2],[3,4]]
puts squash(arr)

