#!/home/hz/.rbenv/shims/ruby

def takeOneDown(beers)
  return if beers <= 0
  if beers == 1
    puts "#{beers} bottle of beer on the wall, #{beers} bottle of beer..."
    puts "You take one down, and pass it around..."
    puts "#{beers - 1} bottles of beer on the wall!"
    takeOneDown(0)
  else
    puts "#{beers} bottles of beer on the wall, #{beers} bottles of beer..."
    puts "You take one down, and pass it around..."
    puts "#{beers - 1} bottles of beer on the wall!"
    takeOneDown(beers - 1)
  end
end

takeOneDown(99)
