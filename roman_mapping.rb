#!/home/hz/.rbenv/shims/ruby

def toRoman(x, result = "")
  map = {
  1000 => "M",
  900 => "CM",
  500 => "D",
  400 => "CD",
  100 => "C",
  90 => "XC",
  50 => "L",
  40 => "XL",
  10 => "X",
  9 => "IX",
  5 => "V",
  4 => "IV",
  1 => "I"
  }
  return result if x == 0
  map.keys.each do |div|
    quot, mod = x.divmod(div)
    result << map[div] * quot
    return toRoman(mod, result) if quot > 0
  end
end

puts "5 = #{toRoman(5)}"
puts "2138 = #{toRoman(2138)}"}"
