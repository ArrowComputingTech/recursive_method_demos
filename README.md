# These Ruby programs use recursion to output text.

**array_flatten**
Converts list of arrays to one flat array.

**bottles_of_beer**
Counts down from 99 bottles of beer using recursion.

**fibonacci_number**
Displays the value in the array position given of the fibonacci sequence.

**integer_mapping**
Converts roman numerals to integers.

**roman_mapping**
Converts integers to roman numerals.
