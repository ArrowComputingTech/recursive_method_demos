#!/home/hz/.rbenv/shims/ruby

def toInt(str, result = 0)
  map = {
  "M" => 1000,
  "CM" => 900,
  "D" => 500,
  "CD" => 400,
  "C" => 100,
  "XC" => 90,
  "L" => 50,
  "XL" => 40,
  "X" => 10,
  "IX" => 9,
  "V" => 5,
  "IV" => 4,
  "I" => 1
  }
  return result if str.empty?
  map.keys.each do |mcdxliv|
    if str.start_with?(mcdxliv)
      result += map[mcdxliv]
      str = str.slice(mcdxliv.length, str.length)
      return toInt(str, result)
    end
  end
end

puts "V = #{toInt('V')}"
puts "MMCXXXVIII = #{toInt("MMCXXXVIII")}"
